# Garden State gs-events-proto
Protocol buffer implemented in Go to serialize events data.
[Protobuf](https://developers.google.com/protocol-buffers/docs/gotutorial)

### Compile proto file
```
$ go get -u google.golang.org/grpc
$ go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
$ protoc events.proto --go_out=plugins=grpc:.
```
